/* Créez une classe Produit :

nom
prix

Créez une classe Panier avec :

une méthode ajoute( produit )
une méthode retire( produit )
une proprieté totalHT
une proprieté totalTTC

Utilisation : ajouter ce qu'il faut à ce code de base pour qu'il fonctionne.
    var baguette = new Produit( 'Baguette', 0.85); // prix HT
    var croissant = new Produit( 'Croissant', 0.80);

    var panier = new Panier();
    panier.ajoute(baguette);
    panier.ajoute(croissant);

    console.log(panier.totalHT);
    console.log(panier.totalTTC);   
Bonus : reprendre cet exemple en ajoutant une classe Viennoiserie qui hérite de la classe Produit en lui ajoutant l'attribut booléen frais.*/

function Produit(nom, prix) {
    this.nom = nom;
    this.prix = prix;
}

Viennoiserie.prototype = Object.create(Produit);
Viennoiserie.prototype.constructor = Viennoiserie;

function Viennoiserie(nom, prix, frais) {
    Produit.call(this, nom, prix);
    this.frais = frais;
}

function Panier() {
    this.items = [];
    this.ajoute = function(produit) {
        this.items.push(produit);
    }
    this.retire = function(produit) {
        this.items.splice(this.items.indexOf(produit), 1);
    }
    this.totalHT = 0;
    this.totalTTC = 0;
}


var baguette = new Produit( 'Baguette', 0.85); // prix HT
var croissant = new Produit( 'Croissant', 0.80);
var eclair = new Viennoiserie( 'Éclair', 1.40, true);

var panier = new Panier();
panier.ajoute(baguette);
panier.ajoute(croissant);
panier.ajoute(croissant);
panier.ajoute(croissant);
panier.retire(croissant);
panier.ajoute(eclair);
panier.ajoute(eclair);
panier.ajoute(eclair);
panier.retire(eclair);

for (nbItems in panier.items) {
    panier.totalHT += panier.items[nbItems].prix;
}

var TVA = panier.totalHT * (5.5 / 100);
panier.totalTTC = panier.totalHT + TVA;

console.log(panier);

console.log(panier.totalHT.toFixed(2));
console.log(panier.totalTTC.toFixed(2));