/* Créez une classe Chrono avec :

- une méthode start()
- une méthode pause()
- une méthode stop()
- une propriété currentTime

Indices :

setTimeout : https://developer.mozilla.org/fr/docs/Web/API/WindowTimers/setTimeout
setInterval : https://developer.mozilla.org/fr/docs/Web/API/WindowTimers/setInterval

Bonus : Mesurer le temps que met le chrono pour écouler 5 min. Même chose pour 10 min. Comment réduire l'écart entre le temps affiché et le temps réel? */

var compteur = document.getElementById("compteur");
var lancer = document.getElementById("lancer");
var pause = document.getElementById("pause");
var arreter = document.getElementById("arreter");

var intervalChrono;

compteur.innerHTML = 0;
var caTourne = false;

var Chrono = function() {
	var chrThis = this;
	this.currentTime = 0;
	this.start = function() {
	if (caTourne === false) {
		caTourne = true;
		intervalChrono = setInterval(changeHTML, 1000);
	}
	}
	this.pause = function() {
		caTourne = false;
		clearInterval(intervalChrono);
	}
	this.stop = function() {
		caTourne = false;
		clearInterval(intervalChrono);
		chrThis.currentTime = 0;
		compteur.innerHTML = chrThis.currentTime;
	}
}

var chrono = new Chrono();

function changeHTML() {
	chrono.currentTime++;
	compteur.innerHTML = chrono.currentTime;
}

lancer.addEventListener("click", chrono.start);
pause.addEventListener("click", chrono.pause);
arreter.addEventListener("click", chrono.stop);
