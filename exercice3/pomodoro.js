"use strict";

const tache = document.getElementById("tache");
const compteur = document.getElementById("compteur");
const lancer = document.getElementById("lancer");
const pause = document.getElementById("pause");
const stop = document.getElementById("stop");
const tacheEnCours = document.getElementById("tacheEnCours");
const listeTaches = document.getElementById("listeTaches");

let intervalChrono;

let caTourne = false;
let tempsDePause = false;
let limiteAvantGrandePause = 1; // Cette variable me sert à vérifier si j'ai fait un cycle de 4 sprints
let totalSprints = 0;
let itemListe = [];

let Chrono = function() {
	let chrThis = this;
	this.currentTime = 1500;
	this.secondes;
	this.minutes;
	this.start = function() {
		if (caTourne === false && tache.value !== "") {
			caTourne = true;
			intervalChrono = setInterval(changeHTML, 1000);
			tacheEnCours.innerHTML = `Tâche en cours : ${tache.value}`;
		}
	};
	this.pause = function() {
		caTourne = false;
		clearInterval(intervalChrono);
	};
	this.stop = function() {
		caTourne = false;
		clearInterval(intervalChrono);
		chrThis.currentTime = 1500;
		chrThis.secondes = chrThis.currentTime % 60;
		chrThis.minutes = (chrThis.currentTime - chrThis.secondes) / 60;
		compteur.innerHTML = `${chrThis.minutes}:${("0" + chrThis.secondes)
			.toString()
			.slice(-2)}`;
	};
};

let pomodoro = new Chrono();

function changeHTML() {
	if (pomodoro.currentTime !== 0) {
		pomodoro.currentTime--;
		pomodoro.secondes = pomodoro.currentTime % 60;
		pomodoro.minutes = (pomodoro.currentTime - pomodoro.secondes) / 60;
		compteur.innerHTML = `${pomodoro.minutes}:${("0" + pomodoro.secondes)
			.toString()
			.slice(-2)} ${tempsDePause === true ? "(Pause)" : ""}`;
	} else if (
		// Petite pause
		pomodoro.currentTime === 0 &&
		tempsDePause === false &&
		limiteAvantGrandePause < 4
	) {
		tacheEnCours.innerHTML = "On souffle un peu";
		pomodoro.currentTime = 300;
		tempsDePause = true;
		itemListe.push(document.createElement("li"));
		itemListe[totalSprints].innerHTML = tache.value;
		listeTaches.appendChild(itemListe[totalSprints]);
		totalSprints++;
	} else if (
		// On se remet au boulot
		pomodoro.currentTime === 0 &&
		tempsDePause === true &&
		limiteAvantGrandePause < 4
	) {
		tacheEnCours.innerHTML = `Tâche en cours : ${tache.value}`;
		limiteAvantGrandePause++;
		pomodoro.currentTime = 1500;
		tempsDePause = false;
	} else if (
		// Grande pause
		pomodoro.currentTime === 0 &&
		tempsDePause === false &&
		limiteAvantGrandePause === 4
	) {
		tacheEnCours.innerHTML = "Une pause bien méritée";
		limiteAvantGrandePause = 1;
		pomodoro.currentTime = 900;
		tempsDePause = true;
		itemListe.push(document.createElement("li"));
		itemListe[totalSprints].innerHTML = tache.value;
		listeTaches.appendChild(itemListe[totalSprints]);
		totalSprints++;
	}
}

lancer.addEventListener("click", pomodoro.start);
pause.addEventListener("click", pomodoro.pause);
stop.addEventListener("click", pomodoro.stop);
